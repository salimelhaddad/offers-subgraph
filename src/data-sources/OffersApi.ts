import { RESTDataSource } from "@apollo/datasource-rest";

export default class OffersApi extends RESTDataSource {
  constructor() {
    super();
    this.baseURL = process.env.OFFERS_URL;
  }

  getCommercialOffers(bookRefs: string) {
    return this.get(`/books/${bookRefs}/commercialOffers`);
  }
}
