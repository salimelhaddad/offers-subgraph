export type BookType = {
  isbn?: string | null;
  title?: string | null;
  price?: number | null;
  cover?: string | null;
  synopsis?: string | null;
};
