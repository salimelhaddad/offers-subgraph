import { ApolloServer } from "@apollo/server";
import { startStandaloneServer } from "@apollo/server/standalone";
import { buildSubgraphSchema } from "@apollo/subgraph";
import dotenv from "dotenv";
import { readFileSync } from "fs";
import gql from "graphql-tag";

import resolvers from "./resolvers";
import OffersApi from "./data-sources/OffersApi";

const environment = process.env.NODE_ENV || "development";
const envFilePath = `.env.${environment}`;
dotenv.config({ path: envFilePath });

const typeDefs = gql(
  readFileSync("./src/offers.graphql", { encoding: "utf-8" })
);

const startApolloServer = async () => {
  const server = new ApolloServer({
    schema: buildSubgraphSchema({ typeDefs, resolvers: resolvers as any }),
  });

  const subgraphName = "reviews";

  try {
    const { url } = await startStandaloneServer(server, {
      context: async () => {
        return {
          dataSources: {
            offersApi: new OffersApi(),
          },
        };
      },
      listen: { port: +process.env.PORT_NUMBER! },
    });

    console.log(`🚀 Subgraph ${subgraphName} running at ${url}`);
  } catch (err) {
    console.error(err);
  }
};

startApolloServer();
