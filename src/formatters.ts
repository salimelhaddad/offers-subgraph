export const concatenateBookRefs = (bookRefs: string[]) => bookRefs.join(",");
