import { concatenateBookRefs } from "./formatters";
import { Resolvers } from "./generated/graphql";

const resolvers: Resolvers = {
  Query: {
    async commercialOffers(obj, { bookRefs }, { dataSources }) {
      const concatenated = concatenateBookRefs(bookRefs);
      try {
        const { offers } = await dataSources.booksApi.getCommercialOffers(
          concatenated
        );
        return offers;
      } catch (e) {
        console.log(e, "error");
      }
      return [];
    },
  },
};

export default resolvers;
